from flask import Flask, render_template, request
# middlewares
from Post.Utils.middlewares import (
    Is_Exist_Username, Is_Token_Valid
)

# function apis
from Post.index import (
    User_List_save, info_user_token, create_post,
    info_post, Login, logout, list_post_page,
    list_post_user_page
)

app = Flask(__name__)

# apis
@app.route('/apis/login', methods=['GET', 'POST'])
def apis5():
    return Login(request)

@app.route('/apis/logout', methods=['GET', 'POST'])
def apis6():
    return logout(request)

@app.route('/apis/user', methods=['GET', 'POST'])
@Is_Exist_Username
def apis():
    return User_List_save(request)

@app.route('/apis/auth/user', methods=['GET', 'POST'])
def apis2():
    return info_user_token(request)

@app.route('/apis/post/user/<int:pag>/<int:limit>', methods=['GET', 'POST'])
@Is_Token_Valid
def apis8(pag, limit):
    return list_post_user_page(request, pag, limit)

@app.route('/apis/post/page/<int:pag>/<int:limit>', methods=['GET', 'POST'])
def apis7(pag, limit):
    return list_post_page(request, pag, limit)

@app.route('/apis/post/<id>', methods=['GET', 'POST', 'DELETE', 'PUT'])
@Is_Token_Valid
def apis4(id):
    return info_post(request, id)

@app.route('/apis/post', methods=['GET', 'POST'])
@Is_Token_Valid
def apis3():
    return create_post(request)


#Return views
@app.route('/')
def main():
    return render_template('index.html')

# all routes response react
@app.route('/<path:subpath>')
def path_all(subpath):
    return render_template('index.html')


if __name__ == '__main__':
    app.run(debug=True)
