var webpack = require('webpack');
module.exports = {
    entry: "./React/app.js",
    output: {
        path: __dirname + '/static',
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }, // for files binary img
            {
                test: /\.(gif|svg|jpg|png)$/,
                loader: "file-loader?name=/img/[name].[ext]"
            },
        ]
    },
};
