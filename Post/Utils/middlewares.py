from flask import request
from functools import wraps
from .index import response_json, Is_Logged
from ..Db.querys import Is_Exist_Username_db

def Is_Exist_Username(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        username = request.get_json()
        username = username['user_name']
        if len(Is_Exist_Username_db(username)) > 0:
            return response_json(False, 'Nombre de Ususario ya ocupado, por favor ingrese otro')
        return f(*args, **kwargs)
    return decorated_function

def Is_Token_Valid(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.method != 'GET':
            try:
                token= request.headers['X-Access-Token']
                is_valid= Is_Logged(token)
                if not is_valid:
                    return response_json(False, {'invalidToken': True})
            except Exception as e:
                return response_json(False, {'invalidToken': True})
        return f(*args, **kwargs)
    return decorated_function