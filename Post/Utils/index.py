from flask import json
import jwt
from ..Db.querys import Is_Logged_Query, get_collection_username_password


secret_key= 'm4dG5ucojQUnS7tXIkHJsceCSrGzC0GJ_(n2d-iszk9k#k=a529l(&n&gq17)renfxr)jjgag2a^7iiex_'

def response_json(success, data):
    return json.dumps(
        {
            'success': success,
            'data': data,
        }
    )

def clean_query_ids(query):
    for index in range(len(query)):
        query[index]['_id'] = str(query[index]['_id'])
        if 'user' in query[index]:
            query[index]['user'] = str(query[index]['user'])
 
    return query

def generate_token(id):
    encoded= jwt.encode({'data':id},secret_key, algorithm='HS384')
    string= encoded.decode("utf-8")
    return string

def decoding_token(token):
    decodign = jwt.decode(token,secret_key,algorithm='HS384')
    return decodign['data']

def Is_Logged(token):
    result = Is_Logged_Query(token)
    if type(result) == type({}):
        return True
    else:
        return False

def is_exist_user_get_id(user_name, password):
    data= get_collection_username_password(user_name, password)
    if (len(data) > 0):
        info= clean_query_ids(data)
        obj= info[0]
        return obj['_id']
    else:
        return False