from pymongo import MongoClient

Host = 'localhost'
Port = 27017
Name = 'Post'

def Post_Db():
    try:
        client = MongoClient(Host, Port)
        schema = client[Name]
        return schema
    except Exception as e:
        return False