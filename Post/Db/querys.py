from .index import Post_Db
import pymongo

def Save_Info_One(collection, info):
    Db = Post_Db()
    result= {}
    if not Db:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        save = Db[collection].insert_one(info)
        if save.inserted_id:
            result= {'success': True, 'data': str(save.inserted_id)}
        else:    
            result= {'success': False, 'data': 'No pudo insertar'}
    return result

def Is_Exist_Username_db(username):
    user = Post_Db()
    if not user:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        result = []
        verify = user['user'].find({"user_name": username})
        for u in verify:
            result.append(u)
        return result
    
def Is_Logged_Query(token):
    db = Post_Db()
    if not db:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        result = []
        verify = db['session'].find({"token": token})
        for u in verify:
            result.append(u)
        return result[0]

def search_collection(collection, info):
    db = Post_Db()
    if not db:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        result = []
        verify = db[collection].find(info)
        for u in verify:
            result.append(u)
        return result
    
def join_generate_filter_post(
    id_post, key_local, table_external, key_external, output
    ):
    db = Post_Db()
    if not db:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        result= db.post.aggregate(
            [
                { "$match": { "_id": id_post}},
                { "$lookup": {
                    "from": table_external,
                    "localField": key_local,
                    "foreignField": key_external,
                    "as": output
                    }
                }
            ]
        )
        return result

def get_collection_username_password(user_name, password):
    db = Post_Db()
    if not db:
        return {'success': False, 'data': 'Error al conectarse a la Bd'}
    else:
        data=[]
        result= db.user.find({
            "user_name": user_name,
            "password": password
        })
        for d in result:
            data.append(d)
        return data

def close_session(collection, info):
    db = Post_Db()
    data= False
    if not db:
        print('error al conectar')
    else:
        result= db[collection].delete_one(info)
        if result.deleted_count >= 1:
            data= True
    return data

def get_collection_paginate(collection, skip, limit, info):
    db = Post_Db()
    data= []
    if not db:
        print('error al conectar')
    else:
        result= db[collection].find(info).sort('_id', pymongo.DESCENDING).skip(skip).limit(limit)
        for a in result:
            data.append(a)
    return data

def delete_collection_id(collection, id_key):
    db = Post_Db()
    if not db:
        print('error al conectar')
    else:
        result= db[collection].delete_one({"_id": id_key})
        return result

def update_one_where(collection, where, update):
    db = Post_Db()
    if not db:
        print('error al conectar')
    else:
        result= db[collection].update_one(
            { '_id': where },
            { "$set": update }
        )
        return result