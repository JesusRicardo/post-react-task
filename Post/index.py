from .Utils.index import response_json
from .Db.querys import (
    Save_Info_One, search_collection, join_generate_filter_post,
    close_session, get_collection_paginate, delete_collection_id,
    update_one_where
)
from .Utils.index import (
    generate_token, Is_Logged, decoding_token, clean_query_ids,
    is_exist_user_get_id
)
from bson.objectid import ObjectId
import math

def User_List_save(request):
    if request.method == 'POST':
        object_data = request.get_json()
        save = Save_Info_One('user',object_data)
        if save['success']:
            jwt = generate_token(save['data'])
            token = Save_Info_One('session', {'token': jwt})
            if token['success']:
                return response_json(True, {'token': jwt})
            else:
                return response_json(False, 'Registro exitoso error al iniciar session')    
        else:
            return response_json(save['success'], save['data'])
    else:
        return response_json(False, 'Metodo incorrecto')

def Login(request):
    if (request.method == 'POST'):
        credential= request.get_json() 
        is_valid= is_exist_user_get_id(credential['user_name'], credential['password'])
        if not is_valid:
            return response_json(False, 'username o password invalido')
        else:
            jwt = generate_token(is_valid)
            token = Save_Info_One('session', {'token': jwt})
            if token['success']:
                return response_json(True, {'token': jwt})
            else:
                return response_json(False, 'error al iniciar session')
    else:
        return response_json(False, 'Metodo incorrecto')

def info_user_token(request):
    if request.method == 'GET':
        token = request.headers['X-Access-Token']
        verify = Is_Logged(token)
        if verify:
            get_info= search_collection('user', {'_id': ObjectId(decoding_token(token))})
            clean= clean_query_ids(get_info)
            return response_json(True, clean[0])
        else:
            return response_json(False, 'No iniciado sesison')
    else:
        return response_json(False, 'Metodo incorrecto')

def create_post(request):
    if (request.method == 'POST'):
        token= request.headers['X-Access-Token']
        decoding= decoding_token(token)
        json=request.get_json()
        json.setdefault('user',ObjectId(decoding))
        save= Save_Info_One('post', json)
        return response_json(save['success'], save['data'])        
    else:
        return response_json(False, 'Metodo incorrecto')
    
def info_post(request, id):
    if (request.method == 'GET'):
        search= join_generate_filter_post(
            ObjectId(id), 'user',
            'user', '_id', 'info_user'
        )
        response= {}
        for item in search:
            response = item
        if len(response) > 0:
            del response['_id']
            del response['user']
            info_user= clean_query_ids(response['info_user'])
            info_user= info_user[0]
            del info_user['_id']
            del info_user['password']
            del info_user['user_name']
            response['info_user']= info_user
            return response_json(True, response)
        else:
            return response_json(False, 'Este Post No Existe')
    elif request.method == 'DELETE':
        result = delete_collection_id("post", ObjectId(id))
        if result.deleted_count == 0:
            return response_json(False, 'Este post no existe')
        else:
            return response_json(True, 'Post borrado exitosamente')
    elif request.method == 'PUT':
        info= request.get_json()
        data=info['info']
        result= update_one_where('post', ObjectId(id), data)
        if isinstance(result.modified_count, int):
            return response_json(True, 'Actualizacion completa')
        else:
            return response_json(False, 'No actualizacion')
    else:
        return response_json(False, 'Metodo incorrecto')
    
def logout(request):
    if request.method == 'POST':
        token = request.headers['X-Access-Token']
        isResult = close_session('session', {'token': token})
        if isResult:
            return response_json(True, 'Session Cerrada')
        else:
            return response_json(False, 'No se pudo cerrar session')
    else:
        return response_json(False, 'Metodo incorrecto')

def list_post_page(request, page, limit):
    if request.method == 'GET':
        tam= len(search_collection('post', {}))
        skip= limit * (page-1)
        list_page= get_collection_paginate('post', skip,limit, {})
        list_page= clean_query_ids(list_page)
        for i in range(len(list_page)):
            obj= list_page[i]
            del obj['user']
            list_page[i]= obj
        
        if len(list_page) > 0:
            return response_json(True, {
                'listPost': list_page,
                'tam': tam,
            })
        else:
            return response_json(False, 'No hay contenido')
    else:
        return response_json(False, 'metodo no valido')

def list_post_user_page(request, page, limit):
    if request.method == 'GET':
        #Get token and decoding uid
        token= request.headers['X-Access-Token']
        decoding= decoding_token(token)
        #calculate size collection 'post' and paginate-filter post for user and limit
        tam= len(search_collection('post', {}))
        skip= limit * (page-1)
        list_page= get_collection_paginate('post', skip,limit, {'user': ObjectId(decoding)})

        if len(list_page) == 0:
            return response_json(False, 'Este usuario No tiene post Creados')
        else:
            return response_json(True,
                {
                    'listPost': clean_query_ids(list_page),
                    'tam': tam,
                }
            )
    else:
        return response_json(False, 'metodo no valido')
