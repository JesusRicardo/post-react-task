import { type } from './index';
import Call from '../../../utils/call';


// Dispachers
export function setCreatePost(data) {
    return {
        payload: data,
        type: type.CREATE_POST,
    };
}
export function setGetIdPost(data) {
    return {
        payload: data,
        type: type.GET_ID_POST,
    }
}
export function setListFullPost(data) {
    return {
        payload: data,
        type: type.LIST_FULL_POST,
    }
}
export function setListFullPostUser(data) {
    return {
        payload: data,
        type: type.LIST_FULL_POST_USER,
    }
}
export function setDeletePostUserId(data) {
    return {
        payload: data,
        type: type.DELETE_POST_USER_ID,
    }
}
export function setUpdatePostUserId(data) {
    return {
        payload: data,
        type: type.UPDATE_POST_USER_ID,
    }
}

// Actions
export function CreatePostCall(title, description, content, date) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call('/apis/post', 'POST', {
                title,
                description,
                content,
                date,
            })
                .then((success) => {
                    dispatch(setCreatePost(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function GetIdPost(id) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call(`/apis/post/${id}`, 'GET')
                .then((success) => {
                    dispatch(setGetIdPost(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function ListFullPost(page=1, limit) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call(`/apis/post/page/${page}/${limit}`, 'GET')
                .then((success) => {
                    dispatch(setListFullPost(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function ListFullPostUser(page=1, limit) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call(`/apis/post/user/${page}/${limit}`, 'GET')
                .then((success) => {
                    dispatch(setListFullPostUser(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function DeletePostUserId(id) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call(`/apis/post/${id}`, 'DELETE')
                .then((success) => {
                    dispatch(setDeletePostUserId(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function UpdatePostUserId(data, id) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call(`/apis/post/${id}`, 'PUT', {info: data})
                .then((success) => {
                    dispatch(setUpdatePostUserId(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}