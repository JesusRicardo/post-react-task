export const type = {
    LIST_FULL_POST: 'LIST_FULL_POST', 
    CREATE_POST: 'CREATE_POST',
    GET_ID_POST: 'GET_ID_POST',
    LIST_FULL_POST_USER: 'LIST_FULL_POST_USER',
    DELETE_POST_USER_ID: 'DELETE_POST_USER_ID',
    UPDATE_POST_USER_ID: 'UPDATE_POST_USER_ID',
};

export function ActionsPost(state = {}, action) {
    const actions = [
        type.CREATE_POST,
        type.DELETE_POST_USER_ID,
        type.UPDATE_POST_USER_ID,
    ];
    return actions.includes(action.type) ? action.payload : state;
}
export function GetPostId(state = {}, action) {
    const actions = [
        type.GET_ID_POST,
    ];
    return actions.includes(action.type) ? action.payload : state;
}
export function ListFullPost(state = {}, action) {
    const actions = [
        type.LIST_FULL_POST,
    ];
    return actions.includes(action.type) ? action.payload : state;
}
export function ListFullPostUser(state = {}, action) {
    const actions = [
        type.LIST_FULL_POST_USER,
    ];
    return actions.includes(action.type) ? action.payload : state;
}