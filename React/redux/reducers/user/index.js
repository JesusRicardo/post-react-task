export const type = {
    CREATE_USER: 'CREATE_USER',
    LOGIN_USER: 'LOGIN_USER',
    CLOSE_LOGIN_USER: 'CLOSE_LOGIN_USER',
    INFO_USER_TOKEN: 'INFO_USER_TOKEN',
};

export function ActionsUser(state = {}, action) {
    const actions = [
        type.CREATE_USER,
        type.LOGIN_USER,
        type.CLOSE_LOGIN_USER,
    ];
    return actions.includes(action.type) ? action.payload : state;
}

export function InfoUserToken(state = {}, action) {
    const actions = [
        type.INFO_USER_TOKEN,
    ];
    return actions.includes(action.type) ? action.payload : state;
}