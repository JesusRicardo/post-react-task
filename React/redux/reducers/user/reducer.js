import { type } from './index';
import Call from '../../../utils/call';


// Dispachers
export function setCreateUser(data) {
    return {
        payload: data,
        type: type.CREATE_USER,
    };
}
export function setInfoUserToken(data) {
    return {
        payload: data,
        type: type.INFO_USER_TOKEN,
    }
}
export function setLoginUser(data) {
    return {
        payload: data,
        type: type.LOGIN_USER,
    }
}
export function setCloseLogin(data) {
    return {
        payload: data,
        type: type.CLOSE_LOGIN_USER,
    }
}

// Actions
export function CreateUser(username, password, firstName, lastName, description) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call('/apis/user', 'POST', {
                user_name: username,
                password,
                first_name: firstName,
                last_name: lastName,
                description
            })
                .then((success) => {
                    dispatch(setCreateUser(success))
                    resolve(success)
                })
                .catch((error) => reject(error))

        })
    }
}
export function InfoUserToken() {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call('/apis/auth/user', 'GET')
                .then((success) => {
                    dispatch(setInfoUserToken(success))
                    resolve(success)
                })
                .catch((error) => reject(error))

        })
    }
}
export function LoginUser(username, password) {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call('/apis/login', 'POST', { user_name: username, password })
                .then((success) => {
                    dispatch(setLoginUser(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
export function CloseLogin() {
    return (dispatch) => {
        return new Promise((resolve, reject) => {
            Call('/apis/logout', 'POST',)
                .then((success) => {
                    dispatch(setCloseLogin(success))
                    resolve(success)
                })
                .catch((error) => reject(error))
        })
    }
}
