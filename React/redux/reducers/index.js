import { ActionsUser, InfoUserToken } from './user/index';
import { ActionsPost, GetPostId, ListFullPost, ListFullPostUser } from './post/index';

export default {
    ActionsUser,
    InfoUserToken,

    ActionsPost,
    GetPostId,
    ListFullPost,
    ListFullPostUser
}