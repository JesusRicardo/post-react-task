// Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';

// Components
import TopBar from '../components/topbar.jsx';
import ListCardView from '../components/list-card-view.jsx';
import Loading from '../themes/loading.jsx'
import Pagination from '../components/pagination.jsx';

//Call
import { ListFullPost } from '../redux/reducers/post/reducer';

class Index extends Component {
    constructor() {
        super();
        this.state = {
            list: {},
            limit: 3,
            pageActual: 1,
            numberAditional: 1,
        }
    }
    componentDidMount() {
        const { limit, pageActual} = this.state;
        this.props.ListFullPost(pageActual, limit)
    }
    static getDerivedStateFromProps(props, state) {
        if (props.list !== state.list) {
            return {
                list: props.list
            };
        }
        return null
    }
    handlePagination(page) {
        this.setState({
            list: {},
            pageActual: page,
        }, () => {
            const { limit, pageActual} = this.state;
            this.props.ListFullPost(pageActual, limit);
        })
    }
    render() {
        return (
            <div className="container">
                <TopBar />
                {
                    Object.keys(this.state.list).length === 0 ?
                        <div className="content-center-flex">
                            <Loading
                                width= '80'
                                height= '80'
                            />
                        </div>
                    : 
                        this.state.list.success ?
                            <div className="content-pagination-list">
                                <div className="content-center">
                                    <div/>
                                    <ListCardView
                                        list={this.state.list.data.listPost}
                                    />
                                    <div/>
                                </div>
                                <div className="content-center">
                                    <div/>  
                                    <Pagination
                                        size={this.state.list.data.tam}
                                        pageActual={this.state.pageActual}
                                        limit={this.state.limit}
                                        numberAditional={this.state.numberAditional}
                                        handlePagination={(page) => this.handlePagination(page)}
                                    />
                                    <div/>
                                </div>
                            </div>
                        : 
                            <div className="content-center">
                                <h3>{this.state.list.data}</h3>
                            </div>
                        
                }
                <style jsx>{`
                    .container {
                        display: grid;
                        grid-template-rows: 10em auto;
                        grid-row-gap: 1em;
                        padding-bottom: 2em;
                    }
                    .content-pagination-list {
                        display: grid;
                        grid-template-rows: auto 2em;
                        grid-row-gap: 1em;
                    }
                    .content-center {
                        display: grid;
                        grid-template-columns: auto 50em auto;
                    }
                    .content-center-flex {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
                `}</style>
            </div>
        )
    }
};
Index.propTypes = {
    ListFullPost: PropTypes.func.isRequired,
    list: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    list: state.ListFullPost,
});
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        ListFullPost,
      },
      dispatch,
    )
  );

export default connect(mapStateToProps, mapDispatchToProps)(Index);
