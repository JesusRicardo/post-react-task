// Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';

// Call
import { InfoUserToken } from '../redux/reducers/user/reducer';

// Components
import Loading from '../themes/loading.jsx';
import TopBar from '../components/topbar.jsx';
import ListPostUser from '../components/list-post-user.jsx';


class Profile extends Component {
    constructor() {
        super();
        this.state = {
            profile: {},
        }
    }
    componentDidMount() {
        this.props.InfoUserToken();
    }
    static getDerivedStateFromProps(props, state) {
        if (props.profile !== state.profile) {
            return {
                profile: props.profile
            };
        }
        return null
    }
    render() {
        return (
            <div className="container">
                <TopBar />
                {
                    Object.keys(this.state.profile).length === 0 ?
                        <div className="content">
                            <div />
                            <Loading
                                width='80'
                                height='80'
                            />
                            <div />
                        </div>
                        :
                        <div className="content">
                            <div />
                            <div className="content-info">
                                <span className="title">datos personales</span>
                                <div className="content-data">
                                    <span
                                        className="description"
                                    >
                                        Nombre:
                                    </span>
                                    <span
                                        className="data"
                                    >
                                        {`${this.state.profile.data.first_name} ${this.state.profile.data.last_name}`}
                                    </span>
                                </div>
                                <div className="content-data">
                                    <span
                                        className="description"
                                    >
                                        User name:
                                    </span>
                                    <span
                                        className="data"
                                    >
                                        {this.state.profile.data.user_name}
                                    </span>
                                </div>
                            </div>
                            <div />
                        </div>
                }
                <div
                    className="content"
                >
                    <div />
                    <ListPostUser />
                    <div />
                </div>
                <style jsx>{`
                    .container {
                        display: grid;
                        grid-template-rows: 10em auto;
                        grid-row-gap: 1em;
                        padding-bottom: 2em;
                    }
                    .content {
                        display: grid;
                        grid-template-columns: 20vw 60vw 20vw;
                    }
                    .content-info {
                        display: grid;
                        grid-template-rows: 5em auto;
                        grid-row-gap: 1em;
                        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    }
                    .title {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 1.5em;
                        background-color: #0080c0;
                        color: white;
                        text-transform: uppercase;
                    }
                    .content-data {
                        display: flex;
                        justify-content: space-evenly;
                        align-items: center;
                    }
                    .description, .data {
                        font-size: 1.5em;
                        text-transform: uppercase;
                    }
                    .description {
                        color: #ee1541;
                    }
                `}</style>
            </div>
        )
    }
}
Profile.propTypes = {
    InfoUserToken: PropTypes.func.isRequired,
    profile: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    profile: state.InfoUserToken,
});
const mapDispatchToProps = dispatch => (
    bindActionCreators(
        {
            InfoUserToken,
        },
        dispatch,
    )
);
export default connect(mapStateToProps, mapDispatchToProps)(Profile);