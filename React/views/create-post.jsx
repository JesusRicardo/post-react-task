// Dependencies
import React, { Component, createRef } from 'react';
import { withRouter } from 'react-router-dom';
import CKEditor from 'react-ckeditor-component';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
 
// Components
import TopBar from '../components/topbar.jsx';
import Loading from '../themes/loading.jsx';
import NotificationError from '../themes/notificationError.jsx';
import NotificationSuccess from '../themes/notificationSuccess.jsx';
 
// styles default
import { inputTheme, areaTheme, buttomSendTheme, labelFormTheme } from '../themes/index';
 
// Call
import { CreatePostCall } from '../redux/reducers/post/reducer';
 
class CreatePost extends Component {
    constructor() {
        super();
        this.title = createRef();
        this.date = null;
        this.state = {
            content: '',
            loading: false,
            error: false,
            success: false,
            errorInfo: '',
            successInfo: '',

            description: '',
        }
    }
    register() {
        this.setState({
            loading: true,
            success: false,
            error: false,
        }, () => {
            const title = this.title.current.value.trim(), description = this.state.description.trim(),
            date = new Date(), content = this.state.content;
            function valid() {
                let bool = false, error = ''; 
                if (title.length === 0 || description.length === 0 || content.length === 0) {
                    error = 'complete los campos';
                } else if (title.length < 3) {
                    error = 'titulo invalido, minimo 3 caracteres';
                } else if (description.length < 10) {
                    error = 'descripcion invalida, minimo 10 caracteres';
                } else if (description.length > 250) {
                    error = 'excedio el tamaño maximo de caracteres (max 250)';
                } else {
                    bool = true;
                }
                return {
                    bool,
                    error,
                }
            }
            const isValid = valid();
            if (isValid.bool) {
                this.props.CreatePostCall(title, description,content,date)
                .then((success) => {
                    if (success.success) {
                        this.props.history.push(`/post/${success.data}`)
                    } else {
                        this.setState({
                            loading: false,
                            error: true,
                            errorInfo: success.data,
                        })
                        setTimeout(() => this.setState({ error: false }), 5000);    
                    }
                })
                .catch((error) => {
                    this.setState({
                        loading: false,
                        error: true,
                        errorInfo: error,
                    })
                    setTimeout(() => this.setState({ error: false }), 5000);    
                })
            } else {
                this.setState({
                    loading: false,
                    error: true,
                    errorInfo: isValid.error,
                })
                setTimeout(() => this.setState({ error: false }), 5000);
            }
        })
    }
    render() {
        const input = [
            {
                name: 'Titulo', type: 'text',  ref: this.title, class: 'inputTheme', for: 'title'
            },
            {
                name: 'Descripcion', type: 'area', ref: this.state.description, class: 'areaTheme', for: 'title'
            }
        ]
        return (
            <div className="container">
                <TopBar />
                <div className="content">
                    <div/>
                    <form className="form-content">
                        <span className="title">crear nuevo post</span>
                        <div className="input-content">
                            {
                                input.map((value, i) => (
                                    
                                        <label
                                            key={i}
                                            className="labelFormTheme"
                                            htmlFor={value.for}
                                        >
                                            {value.name}:
                                            {
                                                value.type === "text" ?
                                                    <input
                                                        className={value.class}
                                                        type={value.type}
                                                        ref={value.ref}
                                                        id={value.for}
                                                    />
                                                :
                                                    <textarea
                                                        className={value.class}
                                                        value={value.ref}
                                                        id={value.for}
                                                        onChange={(e) => {
                                                                const listVecDescription = e.target.value.split(' ');
                                                                if (listVecDescription[listVecDescription.length - 1].length <= 23) {
                                                                    this.setState({
                                                                        description: e.target.value
                                                                    })
                                                                }
                                                        }}
                                                    />
                                            }
                                        </label>
                                    ))
                            }
                            <label
                                className="labelFormTheme"
                            >
                                Contenido:
                                <CKEditor 
                                    activeClass="p10" 
                                    content={this.state.content}
                                    events={{
                                        'change': (evt) => this.setState({content: evt.editor.getData()})
                                    }}
                                />
                            </label>
                        </div>
                        <div className="content-button-loading">
                            <button
                                type="button"
                                className="buttomSendTheme"
                                onClick={() => this.register()}
                            >
                                Guardar
                            </button>
                            {
                                this.state.loading &&
                                <Loading />
                            }
                        </div>
                    </form>
                    <div/>
                    {
                        this.state.error &&
                            <NotificationError
                                info={this.state.errorInfo}
                                close={() => this.setState({error: false})}
                            />
                    }
                    {
                        this.state.success &&
                            <NotificationSuccess
                                info={this.state.successInfo}
                                close={() => this.setState({success: false})}
                            />
                    }
                </div>
                <style jsx>{`
                    .container {
                        display: grid;
                        grid-template-rows: 10em auto;
                        grid-row-gap: 2em;
                    }
                    .content {
                        display: grid;
                        grid-template-columns: 20vw 60vw 20vw;
                    }
                    .form-content {
                        display: grid;
                        grid-template-rows: 5em auto auto;
                        grid-row-gap: 1em;
                        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    }
                    .input-content {
                        display: grid;
                        grid-template-rows: auto;
                        grid-row-gap: 1em;
                        margin: 1em;
                    }
                    .title {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 1.5em;
                        background-color: #0080c0;
                        color: white;
                        text-transform: uppercase;
                    }
                    .content-button-loading {
                        display: flex;
                        justify-content: space-evenly;
                        align-items: center;
                    }
                `}</style>
                <style jsx>{inputTheme}</style>
                <style jsx>{areaTheme}</style>
                <style jsx>{labelFormTheme}</style>
                <style jsx>{buttomSendTheme}</style>
            </div>
        );
    }
}
CreatePost.propTypes = {
    CreatePostCall: PropTypes.func.isRequired,
}
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        CreatePostCall,
      },
      dispatch,
    )
  );
export default connect(null, mapDispatchToProps)(withRouter(CreatePost));
