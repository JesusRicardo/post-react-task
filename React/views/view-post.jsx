//Dependencies
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

//Components
import TopBar from '../components/topbar.jsx';
import Loading from '../themes/loading.jsx';

//Call
import { GetIdPost } from '../redux/reducers/post/reducer';

class ViewPost extends Component {
    constructor() {
        super();
        this.state = {
            post: {},
        }
    }
    componentDidMount() {
        this.props.GetIdPost(this.props.match.params.idPost)
    }
    static getDerivedStateFromProps(props, state) {
        if (props.postInfo !== state.post) {
            return {
                post: props.postInfo,
            };
        }
        return null
    }
    render() {
        let date;
        if (Object.keys(this.state.post).length > 0) {
            date = new Date(this.state.post.data.date);
        };
        return (
            <div className="container">
                <TopBar />
                <div className="content">
                    <div />
                    {
                        Object.keys(this.state.post).length === 0 ?
                            <Loading
                                width='50'
                                height='50'
                            />
                        : 
                            (this.state.post.success) ?
                                <div className="post">
                                    <span className="title">{this.state.post.data.title}</span>
                                    <div
                                        className="content_html"
                                        dangerouslySetInnerHTML={{__html: this.state.post.data.content}}
                                    />
                                    <div className="content_user">
                                        <span
                                            className="info_user"
                                        >{`
                                            ${this.state.post.data.info_user.first_name}
                                            ${this.state.post.data.info_user.last_name}
                                            - ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}
                                        `}</span>
                                        <span
                                            className="description_user"
                                        >{this.state.post.data.info_user.description}</span>
                                    </div>
                                </div>
                            : <h4>{this.state.post.data}</h4>
                    }
                    <div />
                </div>
                <style jsx>{`
                    .container {
                        display: grid;
                        grid-template-rows: 10em auto;
                        grid-row-gap: 2em;
                    }
                    .content {
                        display: grid;
                        grid-template-columns: 10vw 80vw 10vw;
                    }
                    .post {
                        display: grid;
                        grid-template-rows: 5em auto auto;
                        grid-row-gap: 1em;
                        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    }
                    .title {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 1.5em;
                        background-color: #0080c0;
                        color: white;
                        text-transform: uppercase;
                    }
                    .content_html {
                        padding: 0 1em 1em 1em;
                    }
                    .content_user {
                        display: grid;
                        grid-template-rows: auto auto;
                        padding-right: 1em;
                        padding-bottom: 1em;
                    }
                    .info_user, .description_user {
                        display: flex;
                        justify-content: flex-end;
                        align-items: center;
                    }
                    .info_user {
                        text-transform: uppercase;
                        font-style: oblique;
                    }
                    .description_user {
                        text-transform: capitalize;
                    }
                `}</style>
            </div>
        );
    }
}
ViewPost.propTypes = {
    GetIdPost: PropTypes.func.isRequired,
    postInfo: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
}
const mapStateToProps = state => ({
    postInfo: state.GetPostId,
});
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        GetIdPost,
      },
      dispatch,
    )
  );
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ViewPost));