// Dependencies
import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

// Components
import TopBar from '../components/topbar.jsx';

// Routes
import { LoginRegisterRoutes } from '../routes.jsx';

function LoginRegister(props) {
    return (
        <div className="container">
            <TopBar />
            <div className="content">
                <div className="nav">
                    <div />
                    <div className="nav-content">
                        <Link
                            className={props.location.pathname === "/login" ? "nav-link-select" : "nav-link"}
                            to="/login"
                        >
                            Login
                        </Link>
                        <Link
                            className={props.location.pathname === "/register" ? "nav-link-select" : "nav-link"}
                            to="/register"
                        >
                            Registrarse
                        </Link>
                    </div>
                    <div/>
                </div>
                <div className="nav">
                    <div />
                    {
                        LoginRegisterRoutes
                    }
                    <div />
                </div>
            </div>
            <style jsx>{`
                .container {
                    display: grid;
                    grid-template-rows: 10em auto;
                    grid-row-gap: 2em;
                }
                .content {
                    display: grid;
                    grid-template-rows: 3em auto;
                    grid-row-gap: 1em;
                }
                .nav {
                    display: grid;
                    grid-template-columns: 20vw 60vw 19vw;
                }
                .nav-content {
                    display: grid;
                    grid-template-columns: auto auto;
                }
                div :global(.nav-link), div :global(.nav-link-select) {
                    text-decoration: none;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    font-size: 1.5em;
                }
                div :global(.nav-link) {
                    background-color: white;
                    color: #0080c0;
                    border: 1px solid #0080c0;
                }
                div :global(.nav-link-select), div :global(.nav-link:hover) {
                    background-color: #0080c0;
                    color: white; 
                }
            `}</style>
        </div>
    )
}
LoginRegister.propTypes = {
    location: PropTypes.object,
}

export default withRouter(LoginRegister);