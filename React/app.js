//Dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';

// Store
import ConfigureStore from './redux/ConfigureStore';
const Store  = ConfigureStore();

// Routes
import { MainRoutes } from './routes.jsx';

ReactDOM.render(
    <Provider store={Store}>
        <Router>
            {
                MainRoutes
            }
        </Router>
    </Provider>
, document.getElementById('reactEntry'));

