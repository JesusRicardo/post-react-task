// Dependencies
import React from 'react';
import PropTypes from 'prop-types';

// Components
import CardView from './card-view.jsx';

function ListCardView(props) {
    return (
        <div className="container-list">
            {
                props.list.map(val => (
                    <CardView
                        key={val._id}
                        link={val._id}
                        date={val.date}
                        description={val.description}
                        title={val.title}
                    />
                ))
            }
            <style jsx>{`
                .container-list {
                    display: grid;
                    grid-template-rows: auto;
                    grid-row-gap: 1em;
                }
            `}</style>
        </div>
    )
}
ListCardView.propTypes = {
    list: PropTypes.array.isRequired,
}
export default ListCardView;