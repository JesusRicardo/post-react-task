// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';


const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: this.props.pageActual,
            totalRecords: this.props.size,
            pageLimit: this.props.limit,
            pageNeighbours: this.props.numberAditional,
            totalPages: Math.ceil(this.props.size/this.props.limit)
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (props.pageActual !== state.currentPage) {
            return {
                currentPage: props.pageActual,
            }
        } else if (props.size !== state.totalRecords) {
            return {
                totalRecords: props.size,
                totalPages: Math.ceil(props.size/props.limit)
            }
        } else if (props.limit !== state.pageLimit) {
            return {
                pageLimit: props.limit,
                totalPages: Math.ceil(props.size/props.limit)
            }
        } else if (props.numberAditional !== state.pageNeighbours) {
            return {
                pageNeighbours: props.numberAditional,
            }
        }
        return null
    }
    PageNumbers() {
        const totalPages = this.state.totalPages;
        const currentPage = this.state.currentPage;
        const pageNeighbours = this.state.pageNeighbours;

            /**
         * totalNumbers: the total page numbers to show on the control
         * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
         */
        const totalNumbers = (pageNeighbours * 2) + 3;
        const totalBlocks = totalNumbers + 2;
        if (totalPages > totalBlocks) {
            let pages = [];
      
            const leftBound = currentPage - pageNeighbours;
            const rightBound = currentPage + pageNeighbours;
            const beforeLastPage = totalPages - 1;
      
            const startPage = leftBound > 2 ? leftBound : 2;
            const endPage = rightBound < beforeLastPage ? rightBound : beforeLastPage;
      
            pages = range(startPage, endPage);
      
            const pagesCount = pages.length;
            const singleSpillOffset = totalNumbers - pagesCount - 1;
      
            const leftSpill = startPage > 2;
            const rightSpill = endPage < beforeLastPage;
      
            const leftSpillPage = LEFT_PAGE;
            const rightSpillPage = RIGHT_PAGE;
      
            if (leftSpill && !rightSpill) {
              const extraPages = range(startPage - singleSpillOffset, startPage - 1);
              pages = [leftSpillPage, ...extraPages, ...pages];
            } else if (!leftSpill && rightSpill) {
              const extraPages = range(endPage + 1, endPage + singleSpillOffset);
              pages = [...pages, ...extraPages, rightSpillPage];
            } else if (leftSpill && rightSpill) {
              pages = [leftSpillPage, ...pages, rightSpillPage];
            }
      
            return [1, ...pages, totalPages];
          }
        return range(1, totalPages);
    }
    render() {
        if (this.state.totalPages === 1) return null;

        const currentPage = this.state.currentPage;
        const pages = this.PageNumbers();
        return (
            <ul className="pagination">
                { 
                    pages.map((page, index) => {
                        if (page === LEFT_PAGE) return (
                            <li
                                key={index}
                                className="pageItem"
                                onClick={() => {
                                    this.props.handlePagination(
                                        this.state.currentPage - this.state.pageNeighbours * 2 - 1
                                    );
                                }}
                            >
                               <span aria-hidden="true">&laquo;</span>
                            </li>
                        );
                        if (page === RIGHT_PAGE) return (
                            <li
                                key={index}
                                className="pageItem"
                                onClick={() => {
                                    this.props.handlePagination(
                                        this.state.currentPage + this.state.pageNeighbours * 2 + 1
                                    );
                                }}
                            >
                                <span aria-hidden="true">&laquo;</span>
                            </li>
                        );
                        return (
                            <li
                                key={index}
                                className={currentPage !== page ? 'pageItem' : 'pageItemActive'}
                                onClick={() => {
                                    if (currentPage !== page) {
                                        this.props.handlePagination(page);
                                    }
                                }}
                            >
                                { page }
                            </li>
                        );
                    })
                }
                <style jsx>{`
                    .pagination {
                        list-style: none;
                        display: flex;
                        justify-content: center;
                    }
                    .pageItem, .pageItemActive {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        width: 3em;
                        height: 2em;
                        border-left: 1px solid white;
                    }
                    .pageItem {
                        background-color: #d5ddea;
                    }
                    .pageItem:hover {
                        background-color: #27a3de;
                        color: white;
                        cursor: pointer;
                    }
                    .pageItemActive {
                        background-color: #27a3de;
                        color: white;
                    }
                `}</style>

            </ul>
        );
    }
}

Pagination.propTypes = {
    size: PropTypes.number.isRequired,
    pageActual: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    numberAditional: PropTypes.number.isRequired,
    handlePagination: PropTypes.func.isRequired,
}

export default Pagination;
