// Dependencies
import React from 'react';

function Modal(props) {
  return (
    <div className="modal-section">
        <div />
        { props.children }
        <div />
        <style jsx>{`
          .modal-section {
            display: grid;
            grid-template-rows: 5em auto 5em;

            position: fixed;
            z-index: 1000;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0,0,0,0.6);
          }
        `}</style>
    </div>
  );
}
export default Modal;
