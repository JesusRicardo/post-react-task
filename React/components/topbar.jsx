// Dependencies
import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';

// Components
import Loading from '../themes/loading.jsx';
import NotificationSuccess from '../themes/notificationSuccess.jsx';
import NotificationError from '../themes/notificationError.jsx';

// Utils
import { GetToken, RemoveToken } from '../utils/session';

// Call
import { InfoUserToken, CloseLogin } from '../redux/reducers/user/reducer';

class TopBar extends Component {
    constructor() {
        super();
        this.state = {
            loading: false,
            error: false,
            success: false,
            errorInfo: '',
            successInfo: '',

            infoUser: {},
            open: false,
        }
    }
    componentDidMount() {
        if (GetToken()) {
            this.props.InfoUserToken()
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (props.infoUser !== state.infoUser) {
            return {
                infoUser: props.infoUser
            };
        }
        return null
    }
    closeSession() {
        this.props.CloseLogin()
            .then(({success, data}) => {
                if (success) {
                    RemoveToken()
                    window.location.href = "/";
                } else {
                    this.setState({
                        loading: false,
                        error: true,
                        errorInfo: data,
                    })
                    setTimeout(() => this.setState({ error: false }), 5000); 
                }
            })
            .catch(error => {
                this.setState({
                    loading: false,
                    error: true,
                    errorInfo: error,
                })
                setTimeout(() => this.setState({ error: false }), 5000);
            })
    }
    render() {
        return (
            <div className={this.props.location.pathname !== "/login" && this.props.location.pathname !== "/register" ? "container-grid" : "container-flex"}>
                <div className="content-title">
                    <Link
                        className="title"
                        to="/"
                    >
                        post
                    </Link>
                </div>
                {
                    (Object.keys(this.state.infoUser).length === 0) ? (
                        (this.props.location.pathname !== "/login" && this.props.location.pathname !== "/register") &&
                            <div className="content-session">
                                <Link
                                    className="session"
                                    to="/login"
                                >
                                    iniciar session
                                </Link>
                            </div>
                    ) :
                    <div className="content-session">
                        {
                            this.props.location.pathname !== "/post" &&  
                                <Link
                                    className="session"
                                    to="/post"
                                >
                                    crear post
                                </Link>
                        }
                        <div className="content-session">
                            <Link
                                className="session"
                                to="/profile"
                            >
                                {`${this.state.infoUser.data.first_name} ${this.state.infoUser.data.last_name}`}
                            </Link>
                            <img
                                className="img-session"
                                src='static/img/rotate.png'
                                alt="Close"
                                onClick={() => {
                                    this.setState({
                                        open: !this.state.open
                                    })
                                }}
                            />
                            {
                                this.state.open &&
                                <ul className="content-ul-session">
                                    <li
                                        onClick={() => this.closeSession()}
                                        className="ul-session-item"
                                    >
                                        Cerrar Session
                                        {this.state.loading && <Loading />}
                                    </li>
                                </ul>
                            }
                        </div>
                    </div>
                }
                {
                    this.state.error &&
                        <NotificationError
                            info={this.state.errorInfo}
                            close={() => this.setState({error: false})}
                        />
                }
                {
                    this.state.success &&
                        <NotificationSuccess
                            info={this.state.successInfo}
                            close={() => this.setState({success: false})}
                        />
                }
                <style  jsx>{`
                    @font-face {
                        font-family: title
                        src: url(https://fonts.googleapis.com/css?family=Fredoka+One);
                    }
                    .container-grid {
                        display: grid;
                        grid-template-columns: 50vw auto;
                        padding-right: 1em;                             
                    }
                    .container-flex {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }
                    .container-grid, .container-flex {
                        background-color: #27a3de;
                    }
                    .content-title, .content-session {
                        display: flex;
                        align-items: center;
                    }
                    .content-title {
                        justify-content: flex-end;
                    }
                    .content-session {
                        justify-content: space-evenly
                    }
                    div :global(.title), div :global(.session) {
                        font-family: title;
                        color: white;
                        text-decoration: none;
                        text-transform: uppercase;
                    }
                    div :global(.title) {
                        font-size: 5em;
                    }
                    div :global(.session) {
                        font-size: 1.5em;
                    }
                    .img-session {
                        cursor: pointer;
                    }
                    .content-ul-session {
                        position: absolute;
                        background-color: #f1f1f1;
                        min-width: 160px;
                        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                        z-index: 1;
                        cursor: pointer;
                        top: 6em;
                    }
                    .ul-session-item {
                        color: black;
                        padding: 12px 16px;
                        display: block;
                    }
                    .ul-session-item:hover {
                        background-color: #ddd
                    }
                `}</style>
            </div>
        )
    }
}
TopBar.propTypes = {
    location: PropTypes.object,
    InfoUserToken: PropTypes.func.isRequired,
    infoUser: PropTypes.object.isRequired,
    CloseLogin: PropTypes.func.isRequired,
};
const mapStateToProps = state => ({
    infoUser: state.InfoUserToken,
});
  
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        InfoUserToken,
        CloseLogin,
      },
      dispatch,
    )
  );
  

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TopBar));