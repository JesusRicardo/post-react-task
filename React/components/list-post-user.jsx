// Dependencies
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';

// Components
import Loading from '../themes/loading.jsx'
import Pagination from '../components/pagination.jsx';
import NotificationError from '../themes/notificationError.jsx';
import Modal from '../components/modal.jsx';
import EditPostUser from '../components/edit-post-user.jsx';

//Call
import { ListFullPostUser, DeletePostUserId } from '../redux/reducers/post/reducer';

class ListPostUser extends Component {
    constructor() {
        super();
        this.state = {
            error: false,
            errorInfo: '',

            list: {},
            limit: 5,
            pageActual: 1,
            numberAditional: 1,

            modal: false,
            infoModal: {},
        }
    }
    componentDidMount() {
        const { limit, pageActual } = this.state;
        this.props.ListFullPostUser(pageActual, limit)
    }
    static getDerivedStateFromProps(props, state) {
        if (props.list !== state.list) {
            return {
                list: props.list
            };
        }
        return null
    }
    handlePagination(page) {
        this.setState({
            list: {},
            pageActual: page,
        }, () => {
            const { limit, pageActual} = this.state;
            this.props.ListFullPostUser(pageActual, limit);
        })
    }
    delete(id) {
        this.setState({
            error: false,
        }, () => {
            this.props.DeletePostUserId(id)
                .then((success) => {
                    if (success.success) {
                        const { limit, pageActual} = this.state;
                        this.props.ListFullPostUser(pageActual, limit);
                    } else {
                        this.setState({
                            error: true,
                            errorInfo: success.data,
                        })
                        setTimeout(() => this.setState({ error: false }), 5000);
                    }
                })
                .catch((error) => {
                    this.setState({
                        error: true,
                        errorInfo: error,
                    })
                    setTimeout(() => this.setState({ error: false }), 5000);
                })
        })
    }
    render() {
        return (
            <div className="container">
                {
                    Object.keys(this.state.list).length === 0 ?
                        <Loading />
                    : 
                        this.state.list.success ?
                            <div className="content">
                                <span className="title">Lista de Post</span>
                                <div className="content-table">
                                    <table className="table">
                                        <thead>
                                            <tr>
                                                <th>TITULO</th>
                                                <th>DESCRIPCION</th>
                                                <th>FECHA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.list.data.listPost.map((item) => {
                                                    const date = new Date(item.date);
                                                    return (
                                                        <tr key={item._id}>
                                                            <td>{item.title}</td>
                                                            <td>{item.description}</td>
                                                            <td>{`${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`}</td>
                                                            <td>
                                                                <img
                                                                    className="img"
                                                                    src='static/img/delete.png'
                                                                    alt='delete'
                                                                    onClick={() => this.delete(item._id)}
                                                                />
                                                            </td>
                                                            <td>
                                                                <img
                                                                    className="img"
                                                                    src='static/img/edit.png'
                                                                    alt='editar'
                                                                    onClick={() => this.setState({modal: true, infoModal: item})}
                                                                />
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                    <Pagination
                                        size={this.state.list.data.tam}
                                        pageActual={this.state.pageActual}
                                        limit={this.state.limit}
                                        numberAditional={this.state.numberAditional}
                                        handlePagination={(page) => this.handlePagination(page)}
                                    />
                                </div>
                            </div>
                        : <h4>{this.state.list.data}</h4>
                }
                {
                    this.state.error &&
                        <NotificationError
                            info={this.state.errorInfo}
                            close={() => this.setState({error: false})}
                        />
                }
                {
                    this.state.modal &&
                        <Modal>
                            <EditPostUser
                                info={this.state.infoModal}
                                cancel={() => this.setState({modal: false})}
                                getList={() => {
                                    const { limit, pageActual} = this.state;
                                    this.props.ListFullPostUser(pageActual, limit);
                                }}
                            />
                        </Modal>
                }
                <style jsx>{`
                    .container {
                        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    }
                    .content {
                        display: grid;
                        grid-template-rows: 5em auto;
                        grid-row-gap: 1em;
                    }
                    .title {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 1.5em;
                        background-color: #0080c0;
                        color: white;
                        text-transform: uppercase;
                    }
                    .content-table {
                        padding: 1em;
                        display: grid;
                        grid-template-rows: auto;
                        grid-row-gap: 1em;
                    }
                    .table {
                        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                        border-collapse: collapse;
                        width: 100%;
                    }
                    .table td, .table th {
                        border: 1px solid #ddd;
                        padding: 8px;
                    }
                    .table tr:nth-child(even){background-color: #f2f2f2;}
                    .table tr:hover {background-color: #ddd;}
                    .table th {
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #0c50ef;
                        color: white;
                    }
                    .img {
                        cursor: pointer;
                    }
                    .img:hover {
                        background-color: #0080c0;
                    }
                `}</style>
            </div>
        )
    }
}
ListPostUser.propTypes = {
    ListFullPostUser: PropTypes.func.isRequired,
    list: PropTypes.object.isRequired,

    DeletePostUserId: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    list: state.ListFullPostUser,
});
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        ListFullPostUser,
        DeletePostUserId,
      },
      dispatch,
    )
  );

export default connect(mapStateToProps, mapDispatchToProps)(ListPostUser);