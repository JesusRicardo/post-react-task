// Dependencies
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function CardView(props) {
    const date = new Date(props.date);
    
    return (
        <Link
            to={`/post/${props.link}`}
            className="card-view-link"
        >
            <div className="container-img">img</div>
            <div className="container-info">
                <div className="title-content">
                    <span className="title">{props.title}</span>
                    <span className="date">{`${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`}</span>
                </div>
                <div className="description">
                    {props.description}
                </div>
            </div>
            <style jsx>{`
                :global(.card-view-link) {
                    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    height: 10em;
                    width: 50em;
                    text-decoration: none;
                    display: grid;
                    grid-template-columns: 10em 40em;
                }
                :global(.card-view-link:hover) {
                    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
                }
                .container-img {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
                .container-info {
                    display: grid;
                    grid-template-rows: 3em 7em;
                }
                .title-content {
                    display: flex;
                    justify-content: space-evenly;
                    align-items: center;
                    background-color: #27a3de;
                }
                .title, .date {
                    color: white;
                }
                .title {
                    text-transform: uppercase;
                }
                .description {
                    padding: .5em;
                    color: black;
                }
            `}</style>
        </Link>
    )
}
CardView.propTypes = {
    link: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
}

export default CardView;