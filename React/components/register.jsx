// Dependencies
import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';

// styles default
import { inputTheme, areaTheme, buttomSendTheme } from '../themes/index';

// Components
import Loading from '../themes/loading.jsx';
import NotificationSuccess from '../themes/notificationSuccess.jsx';
import NotificationError from '../themes/notificationError.jsx';

// Call
import { CreateUser } from '../redux/reducers/user/reducer';

class Register extends Component {
    constructor() {
        super();
        this.state = {
           loading: false,
           error: false,
           success: false,
           errorInfo: '',
           successInfo: '',
        }
        // Ref dom
        this.userName =  createRef();
        this.password = createRef();
        this.firstName = createRef();
        this.lastName = createRef();
        this.description = createRef();
    }
    register() {
        this.setState({
            loading: true,
            success: false,
            error: false,
        }, () => {
            const username = this.userName.current.value.trim(), password = this.password.current.value.trim(),
                firstName = this.firstName.current.value.trim(), lastName = this.lastName.current.value.trim(),
                description = this.description.current.value.trim();
            function validate() {
                let bool = false, error = '';
                let notSpacing = /\s/;
                if (username.length === 0 && password.length === 0 && firstName.length === 0) {
                    error = 'Complete los campos';
                } else if (username.length < 5) {
                    error = 'Nombre de usuario corto, minimo 5 caracteres';
                } else if (notSpacing.test(username)) {
                    error = 'Nombre de usuario no puede llevar espacios en blanco';
                } else if (password.length < 6) {
                    error = 'Contraseña invalida, minimo 6 caracteres';
                } else if (firstName.length < 3) {
                    error = 'Nombre invalido';
                } else {
                    bool = true;
                }
                return { 
                    bool,
                    error,
                }
            }
            const isvalid = validate();
            if (isvalid.bool) {
                this.props.CreateUser(username, password, firstName, lastName, description)
                    .then((success) => {
                        if (success.success) {
                            location.href = "/"
                        } else {
                            this.setState({
                                loading: false,
                                error: true,
                                errorInfo: success.data,
                            })
                            setTimeout(() => this.setState({ error: false }), 5000);    
                        }
                    })
                    .catch((error) => {
                        this.setState({
                            loading: false,
                            error: true,
                            errorInfo: error,
                        })
                        setTimeout(() => this.setState({ error: false }), 5000);
                    })
            } else {
                this.setState({
                    loading: false,
                    error: true,
                    errorInfo: isvalid.error,
                })
                setTimeout(() => this.setState({ error: false }), 5000);
            }
        })
    }
    render() {
        const input = [
            { type: "text", ref: this.userName, plascheolder: "Nombre de Usuario", theme: "inputTheme" },
            { type: "password", ref: this.password, plascheolder: "Contraseña", theme: "inputTheme" },
            { type: "text", ref: this.firstName, plascheolder: "Nombre", theme: "inputTheme" },
            { type: "text", ref: this.lastName, plascheolder: "Apellido", theme: "inputTheme" },
            { type: "area", ref: this.description, plascheolder: "Ingrese una Descripcion", theme: "areaTheme" },
        ]
        return (
            <form className="content-form">
                {
                    input.map((val, i) => (
                        val.type !== 'area' ?
                            <input
                                key={i}
                                className={val.theme}
                                type={val.type}
                                ref={val.ref}
                                placeholder={val.plascheolder}
                            />
                            :
                            <textarea
                                key={i}
                                className={val.theme}
                                ref={val.ref}
                                placeholder={val.plascheolder}
                            />
                    ))
                }
                <div className="content-button-loading">
                    <button
                        type="button"
                        className="buttomSendTheme"
                        onClick={() => this.register()}
                    >
                        Guardar
                    </button>
                    {
                        this.state.loading &&
                        <Loading />
                    }
                </div>
                {
                    this.state.error &&
                        <NotificationError
                            info={this.state.errorInfo}
                            close={() => this.setState({error: false})}
                        />
                }
                {
                    this.state.success &&
                        <NotificationSuccess
                            info={this.state.successInfo}
                            close={() => this.setState({success: false})}
                        />
                }
                <style jsx>{`
                    .content-form {
                        display: grid;
                        grid-templates-rows: auto;
                        grid-row-gap: 1em;
                        padding: 1em;
                        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
                    }
                    .content-button-loading {
                        display: flex;
                        justify-content: space-evenly;
                        align-items: center;
                    }
                `}</style>
                <style jsx>{inputTheme}</style>
                <style jsx>{areaTheme}</style>
                <style jsx>{buttomSendTheme}</style>
            </form>
        );
    }
}
Register.propTypes = {
    CreateUser: PropTypes.func.isRequired,
}
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        CreateUser,
      },
      dispatch,
    )
  );
  
export default connect(null, mapDispatchToProps)(Register);