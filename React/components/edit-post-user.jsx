// Dependencies
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import CKEditor from 'react-ckeditor-component';

// components
import Loading from '../themes/loading.jsx';
import NotificationError from '../themes/notificationError.jsx';
import NotificationSuccess from '../themes/notificationSuccess.jsx';

// styles default
import { inputTheme, areaTheme, buttomSendTheme, labelFormTheme, buttomCancelTheme } from '../themes/index';

// call
import { UpdatePostUserId } from '../redux/reducers/post/reducer';

class EditPostUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            error: false,
            errorInfo: '',

            post: this.props.info
        }
    }
    handle(value, name) {
        const post = Object.assign({}, this.state.post);
        post[name] = value;
        this.setState({post})
    }
    update() {
        this.setState({
            loading: true,
            error: false,
        }, () => {
            const title = this.state.post.title.trim(), description = this.state.post.description.trim(),
            content = this.state.post.content;
            function valid() {
                let bool = false, error = ''; 
                if (title.length === 0 || description.length === 0 || content.length === 0) {
                    error = 'complete los campos';
                } else if (title.length < 3) {
                    error = 'titulo invalido, minimo 3 caracteres';
                } else if (description.length < 10) {
                    error = 'descripcion invalida, minimo 10 caracteres';
                } else if (description.length > 250) {
                    error = 'excedio el tamaño maximo de caracteres (max 250)';
                } else {
                    bool = true;
                }
                return {
                    bool,
                    error,
                }
            }
            const isValid = valid();
            if (isValid.bool) {
                const post = Object.assign({}, this.state.post);
                const id = post._id;
                delete post._id;
                delete post.user;
                this.props.UpdatePostUserId(post, id)
                    .then((result) => {
                        if (result.success) {
                            this.props.getList();
                            this.props.cancel();
                        } else {
                            this.setState({
                                loading: false,
                                error: true,
                                errorInfo: result.data,
                            })
                            setTimeout(() => this.setState({ error: false }), 5000);
                        }
                    })
                    .catch((error) => {
                        this.setState({
                            loading: false,
                            error: true,
                            errorInfo: error,
                        })
                        setTimeout(() => this.setState({ error: false }), 5000);
                    })
            } else {
                this.setState({
                    loading: false,
                    error: true,
                    errorInfo: isValid.error,
                })
                setTimeout(() => this.setState({ error: false }), 5000);
            }
        })
    }
    render() {
        const input = [
            {
                name: 'Titulo', type: 'text', class: 'inputTheme', for: 'title'
            },
            {
                name: 'Descripcion', type: 'area', class: 'areaTheme', for: 'title'
            }
        ]
        return (
            <div className="content">
                <div />
                <form className="form-content">
                    <span className="title">editar post</span>
                    <div className="input-content">
                        {
                            input.map((value, i) => (

                                <label
                                    key={i}
                                    className="labelFormTheme"
                                    htmlFor={value.for}
                                >
                                    {value.name}:
                                                {
                                        value.type === "text" ?
                                            <input
                                                className={value.class}
                                                type={value.type}
                                                id={value.for}
                                                value={this.state.post.title}
                                                onChange={(e) => {
                                                    this.handle(e.target.value, 'title')
                                                }}
                                            />
                                            :
                                            <textarea
                                                className={value.class}
                                                value={this.state.post.description}
                                                id={value.for}
                                                onChange={(e) => {
                                                    const listVecDescription = e.target.value.split(' ');
                                                    if (listVecDescription[listVecDescription.length - 1].length <= 23) {
                                                        this.handle(e.target.value, 'description')
                                                    }
                                                }}
                                            />
                                    }
                                </label>
                            ))
                        }
                        <label
                            className="labelFormTheme"
                        >
                            Contenido:
                            <CKEditor
                                activeClass="p10"
                                content={this.state.post.content}
                                events={{
                                    'change': (evt) => {
                                        const post = Object.assign({}, this.state.post)
                                        post['content'] = evt.editor.getData()
                                        this.setState({post})
                                    }
                                }}
                            />
                        </label>
                    </div>
                    <div className="content-button-loading">
                        <button
                            type="button"
                            className="buttomCancelTheme"
                            onClick={() => this.props.cancel()}
                        >
                            Cancelar
                        </button>
                        <button
                            type="button"
                            className="buttomSendTheme"
                            onClick={() => this.update()}
                        >
                            Guardar
                                </button>
                        {
                            this.state.loading &&
                            <Loading />
                        }
                    </div>
                </form>
                <div />
                {
                    this.state.error &&
                        <NotificationError
                            info={this.state.errorInfo}
                            close={() => this.setState({error: false})}
                        />
                }
                <style jsx>{`
                    .content {
                        display: grid;
                        grid-template-columns: 5em auto 5em;
                    }
                    .form-content {
                        display: grid;
                        grid-template-rows: 5em auto auto;
                        grid-row-gap: 1em;
                        background-color: white;
                        padding-bottom: 1em;
                    }
                    .input-content {
                        display: grid;
                        grid-template-rows: auto;
                        grid-row-gap: 1em;
                        padding: 1em;
                    }
                    .title {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                        font-size: 1.5em;
                        background-color: #0080c0;
                        color: white;
                        text-transform: uppercase;
                    }
                    .content-button-loading {
                        display: flex;
                        justify-content: space-evenly;
                        align-items: center;
                    }
                `}</style>
                <style jsx>{inputTheme}</style>
                <style jsx>{areaTheme}</style>
                <style jsx>{labelFormTheme}</style>
                <style jsx>{buttomSendTheme}</style>
                <style jsx>{buttomCancelTheme}</style>
            </div>
        )
    }
}
EditPostUser.propTypes = {
    cancel: PropTypes.func.isRequired,
    info: PropTypes.object.isRequired,
    getList: PropTypes.func.isRequired,

    UpdatePostUserId: PropTypes.func.isRequired,
}
const mapDispatchToProps = dispatch => (
    bindActionCreators(
      {
        UpdatePostUserId,
      },
      dispatch,
    )
  );
export default connect(null, mapDispatchToProps)(EditPostUser);