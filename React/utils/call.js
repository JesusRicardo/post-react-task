import { SaveToken, GetToken, RemoveToken } from './session';

function header(url,type,data) {
  const myRequest = new Request(url, {
    method: type,
    headers: {
      "Content-Type": "application/json",
      "x-access-token": GetToken()
    },
    body: JSON.stringify(data) || null,
  })
  return myRequest;
}
function Call(url, type, data) {
  return new Promise((resolve, reject) => {
    fetch(header(url,type,data))
      .then(res => res.json())
      .then(result => {
        resolve(result)
        if (result.data.token) {
          SaveToken(result.data.token)
        }
        if (result.data.invalidToken) {
          RemoveToken();
          window.location.href = "/"
        }
      })
      .catch(() => reject('Error en la peticion, vuelva a intentarlo'))
  })
}

export default Call;