const store = window.localStorage;
const item = 'post_auth_token_id';

export function SaveToken(id) {
    store.setItem(item, id);
}

export function GetToken() {
    return store.getItem(item);
}

export function RemoveToken() {
    return store.removeItem(item);
}