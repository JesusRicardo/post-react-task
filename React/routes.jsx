import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { GetToken } from './utils/session';
// Views
import Index from './views/Index.jsx';
import LoginRegister from './views/login-register.jsx';
import Login from './components/login.jsx';
import Register from './components/register.jsx';
import CreatePost from './views/create-post.jsx';
import Profile from './views/profile.jsx';
import ViewPost from './views/view-post.jsx';


const notAuh = (component) => (GetToken() ? <Redirect to="/" /> : component)
const Auth = (component) => (GetToken() ? component : <Redirect to="/"/>)

export const MainRoutes =  (
    <Switch>
        <Route path="/post/:idPost" component={ViewPost} />
        <Route path="/post" render={() => Auth(<CreatePost />)} />
        <Route path="/profile" render={() => Auth(<Profile />)} />
        <Route path="/register" render={() => notAuh(<LoginRegister />)} />
        <Route path="/login" render={() => notAuh(<LoginRegister />)} />
        <Route exact path="/" component={Index} />
        <Route render={() => <h1>Not Found</h1>} />
    </Switch>
)
export const LoginRegisterRoutes = (
    <Switch>
        <Route path="/register" component={Register} />
        <Route path="/login" component={Login} />
    </Switch>
)